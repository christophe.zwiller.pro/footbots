
#ifndef _FRAMEMANAGER_H_
#define _FRAMEMANAGER_H_

#include <iostream>
#include <fstream>
#include <string>
#include <thread>
#include <mutex>

using namespace std;

#define SIZE_BUFFER		16  
#define SERIAL_FILE		"/dev/serial0"
#define SERIAL_BAUD		115200	//16550


class Frame_manager {
private:
	string fifo[SIZE_BUFFER];
	int idx = 0;

	mutex fifo_lock; 
	thread *thread_serial;

	void remove(void);

public:
	Frame_manager();
	~Frame_manager();

	int		add(const char *str);
	string	get(int i);
	string	getLast(void);   // & remove
	void	print(void);
};


#endif /* _FRAMEMANAGER_H_ */