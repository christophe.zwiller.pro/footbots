#include <stdio.h>
 
/* http://delahaye.emmanuel.free.fr/clib/ */
#include "psleep/inc/psleep.h"
 
int main (void)
{
   int i;
   for (i = 60; i != 0; i--)
   {
      printf ("\rIl vous reste %2d secondes.", i);
      fflush (stdout);
      msleep (1000);
   }
   return 0;
}