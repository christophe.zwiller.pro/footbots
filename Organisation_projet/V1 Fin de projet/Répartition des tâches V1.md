# Répartition des tâches pour la V1 du projet 

## Supervision, organisation : Christophe

-Réalisation d'un nouveau planning à la demande de Mme LE BARON. <br>
-Organisation des tâches pour la V1 à la mi Mai. <br>

## Impression 3D : Romain / Baptiste

-Réalisation d'un nouveau modèle de But plus esthétique avec un système de fixation plus simple si possible. <br>
-Réalisation d'une coque intégrale couvrant tout le robot de façon esthétique pour la présentation de fin de projet et prenant en compte un poids réduit. <br>
-Réalisation d'un support pour la caméra à placer au dessus du terrain au plafond. <br> 

## Traitement caméra : Yves / Kilian - Corentin / Vincent

-Réaliser la détection de la position des robots et de la balle avec une caméra au dessus du terrain à l'aide d'open CV ou autre. <br>
-Transmettre les données de positions. <br>

## Raspberry Pi de contrôle : Tenegan / Simon / Yahn / François - Aurélie - Christophe

-Recevoir les données de position de la caméra et les envoyer à la partie WEB et robots. <br>
-Effectuer un contrôle générale des robots avec du bluetooth multipoint. <br>
-Héberger un serveur web avec l'interface graphique du score. <br>
-Modifier l'interface web pour acceuillir la vidéo de détection des robots. (optionnel) <br>

## Application de contrôle manuel : Rafi / Ismath

-Amélioration de l'application Android studio. <br>
-Ajouter un Joystick pour un meilleur contrôle des robot.(fait) <br>
-Fixer le problème de crash de l'application. <br>
-Ajouter l'asservissement fonctionnel dans le contrôle des robot. <br>

## Robots : Corentin

Robot autonomes : 

-Recevoir les données de déplacements du raspberry pi de contrôle. <br>
-Effectuer un tour pour l'équipe qui vient de marquer (optionnel) <br>

Robot manuels :

-Réaliser un asservissement des moteurs des robots (fait) <br>






