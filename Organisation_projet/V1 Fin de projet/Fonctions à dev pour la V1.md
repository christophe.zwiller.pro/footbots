
# Les fonctionnalités à dev pour la V1 : 
**Echéance : Mi mai**


## Partie Robot/terrain

Terrain : 

Améliorer les buts imprimés en 3D.
Ajouter potentiellement des capteurs de détection de buts.


Robots :

Avoir des robots se déplaçant de manière asservis et autonome en fonction des données du traitement caméra.
Ils doivent être connecté en bluetooth à un raspberry pi de contrôle qui va leur donner les ordres de déplacements.
Les robots doivent tourner sur eux-mêmes lors d'un but.


## Partie Caméra :  

Traiter le flux vidéo de la caméra pour détecter la position des différents types d'objets. (avec openCv par exemple)

## Partie Intelligence Artificielle : 

Envoyer aux robots les informations de déplacements à réaliser afin de mettre des buts et autres.


## Partie IHM : 

Afficher le score en fonction de la détection des buts fourni par soit des capteurs dans les buts soit par le traitement de la caméra.
Emettre des sons lors de but ou autre.
Afficher une représentation du terrain avec la position de la balle en récupérant les coordonnées de la balle.













