
# Les fonctionnalités à dev pour la V0 :

- Match de football avec deux équipes de robots (3 contre 3 robots) avec différents affichages (score, match)
- Durée du match : 2min
- Dans chaque équipe :
    - 1 robot autonome(gardien) qui avance et qui recule par exemple
    - 2 robots contrôlés par deux joueurs avec des tablettes ou autre


## Partie Robot/terrain

Terrain : 
- Avoir des buts imprimés en 3D
- Terrain aux couleurs d'un vrai terrain de foot (poster ou autre scotché sur les planches)
- Capteurs électroniques avec buzzer pour la détection des buts

Robots :
- Avoir une coque intégral pour les 6 robots avec deux couleurs différentes en fonction de l'équipe et un numéro distinctif
- Avoir une partie à l'avant du robot permettant de pousser la balle facilement
- Avoir une commande fonctionnelle des robots par bluetooth avec des tablettes
- Développé une mini intelligence pour que le gardien paraisse "autonome"
- Si le temps, développé un programme permettant de contrôler le robot en fonction des ordres donnée par la partie IA avec la caméra


## Partie Caméra :  
- Avoir une détéction de la position des robots, de la balle et l'afficher sur un écran
- Trouver une solution pour la fixation de la caméra principale au dessus du terrain ( support imprimé en 3D à installé sur le plafond ? )
- Avoir une seconde caméra permettant la diffusion du match et des possibles replay lors de buts


## Partie Intelligence Artificielle : 
- Détecter la position des robots et de la balle
- Si le temps, agir sur le contrôle des robots en fonction de leurs position 


## Partie IHM : 
- Réaliser un visuel du score du match sur écran avec pour l'instant une incrémentation manuel
- Modifier l'application de base de la carte adafruit (pour avoir un meilleur contrôle des mouvements et un meilleur aspect visuel)
(Penser à indiquer le numéro du robot pour que l'utilisateur puisse connaître quel robot il contrôle)
















