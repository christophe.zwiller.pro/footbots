#include "Frame_manager.hpp"
#include "serialib.h"	/*	https://lucidar.me/en/serialib/cross-plateform-rs232-serial-library/		*/

bool      thread_running;
void      thread_func(int *arg);
serialib  serial;

#define DEBUG(x)		cout << x << endl;

Frame_manager::Frame_manager()
{
	/* init serial com */
	if (serial.openDevice("/dev/ttyS0", SERIAL_BAUD)!=1)
	{
		DEBUG("Serial Ko");
	}
	else
	{
		DEBUG("Serial Ok");
	}

	for (idx = SIZE_BUFFER; idx > 0;idx--)
	{
		fifo_lock.lock();
		fifo[idx-1].assign("");
		fifo_lock.unlock();
	}

	idx = -1;

	thread_running = true;
	thread_serial = new thread(thread_func, (int *)this);
}

Frame_manager::~Frame_manager()
{
	/* close serial com */
	serial.closeDevice();

	/* wait close thread */
	thread_running = false;
	thread_serial->join();
}

int Frame_manager::add(const char *str)
{
	fifo_lock.lock();

	if (this->idx < SIZE_BUFFER)
	{
		idx++;
		if (idx > 0) 
			idx = 0;
			
		fifo[idx].assign(str);
	}

	fifo_lock.unlock();
	return (idx);

}

string Frame_manager::get(int i)
{
	if (i < 0)
		return "Error";

	return (fifo[i]);
}

string Frame_manager::getLast(void)
{
	string retstr = "";
	fifo_lock.lock();
    if (idx >= 0)
    {
		retstr.assign(fifo[idx]);
		fifo[idx].assign("");
		idx--;
    }
	fifo_lock.unlock();

	return (retstr);
}

void Frame_manager::remove()
{
	fifo_lock.lock();
	fifo[idx--].assign("");
	fifo_lock.unlock();
}

void Frame_manager::print(void)
{
	cout << "[Frame_manager] index = " << idx << endl;

	for (int i = 0; i < SIZE_BUFFER;i++)
	{
		cout << "\t" << i << " : \"" << fifo[i] << "\"" << endl;
	}
}


void thread_func(int *arg)
{
	string line = "";
	int i_char = 0;
	char buffer[256];
	char read_buffer[256];
	Frame_manager *frame = (Frame_manager *)arg;
	
	while(thread_running)
	{
		if (serial.readChar(read_buffer, 500) > 0)
		{
			//strcpy(buffer[curseur], serieStr);
			if (read_buffer[0] != 0)
			{
				buffer[i_char] = read_buffer[0];
				// printf("\r\n(int)%d, (char)%c\r\n", buffer[i_char], buffer[i_char]);
				
				i_char++;
			}
		}
		else
		{
			cout << "." << flush;
		}

		if ((buffer[i_char-1] == 13) && (i_char > 1))
		{
			buffer[i_char-1] = 0;
			// printf("\r\nFull trame recieved: %s\r\n", buffer);

			int ret = frame->add(buffer);

			/* reset */
			memset (buffer, 0, 256);
			memset (read_buffer, 0, 256);
			i_char = 0;
		}
	}

	cout << "Exit thread" << endl;

}
