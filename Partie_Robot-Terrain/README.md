# Objectifs de cette partie : 

-Réaliser des coques de deux couleurs différentes

-Réaliser un support pour raspberry Pi avec Cam pour mettre aux plafond

-Développer la programmation du robot

-Implémenter une détection de but


## Actuellement :
-Modélisation des coques des robots (à tester)

-Gestion de l'asservissement des gardiens (en cours)

-Réalisation des buts (Fait, à améliorer)

-Modélisation support raspberry pi et cam (en cours)
