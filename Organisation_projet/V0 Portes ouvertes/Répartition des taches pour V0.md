
# Nouvelle Répartition des tâches pour la V0 du projet : 

=> Heures restantes de projet tut avant la V0 : **2 SEANCES de projet tut** 

## Supervision du projet : 
**Christophe**

## buts et accessoires sur le terrain : 
**cHRISTOPHE** : Modélisation pour impression 3D

## Robot(gardien) semi autonome : 
**Corentin** : Robot Gardien qui avance et recule devant les buts

## Boitier Rasp/Cam : 
**Vincent** : Modélisation 3D d'un boitier pour un raspberry pi avec caméra pour les fixer au plafond 

## Coques Robot : 
**Baptiste/Romain** : Modélisation 3D des coques de deux couleurs différentes pour les robots avec une partie incurvé à l'avant pour pousser la balle

## Affichage du score : 
Réalisation d'une interface web pour afficher sur un vidéo projecteur l'affichage du score et le temps dejeu restant

**Aurélie** : Partie Front-end (html/css)

**Yahn/François** : Partie Back-end (javascript/php)

## Appli robot : 
**Rafi/Ismath** : Réalisation d'une application de contrôle manuel des robots sur tablettes

## Caméra avec detection : 
**Yves/Kilian** : Avoir un flux vidéo avec les robots avec une détection des différents robots à l'aide de QrCode

## IA Caméra : 
**Simon/Tenegant** : Développement de la partie IA avec matrice de données pour la suite du projet après V0
(Peuvent aider les autres groupes si besoin)
