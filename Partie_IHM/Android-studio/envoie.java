        //site pour les valeurs : https://www.asciitable.com/
        joystick.setOnJoystickMoveListener(new JoystickView.OnJoystickMoveListener() {

            @Override
            public void onValueChanged(int angle, int power, int direction) {
                // TODO Auto-generated method stub
                angleTextView.setText(" " + String.valueOf(angle) + "°");
                if(mService!=null){
                    byte[] message = new byte[] {(byte) angle};
                    mService.writeRXCharacteristic(message);
                }

            }
        }, JoystickView.DEFAULT_LOOP_INTERVAL);
