#include <ArduinoBLE.h>

BLEService dataService("19B10000-E8F2-537E-4F6C-D104768A1214"); // UUID du service GATT
BLECharacteristic dataChar("19B10001-E8F2-537E-4F6C-D104768A1214", BLERead | BLENotify, 20); // UUID de la caractéristique GATT

void setup() {
  Serial.begin(9600); // Initialisation de la communication série
  while (!Serial);

  if (!BLE.begin()) { // Initialisation du module Bluetooth
    Serial.println("Erreur Bluetooth");
    while (1);
  }

  BLE.setLocalName("Feather BLE"); // Définition du nom du périphérique
  BLE.setDeviceName("Feather BLE"); // Définition du nom du périphérique
  BLE.setAppearance(384); // Définition de l'apparence du périphérique

  BLE.setAdvertisedService(dataService); // Définition du service GATT
  dataService.addCharacteristic(dataChar); // Ajout de la caractéristique GATT
  BLE.addService(dataService); // Ajout du service GATT

  BLE.advertise(); // Démarrage de la publicité Bluetooth
  Serial.println("Attente de connexion...");
}

void loop() {
  BLEDevice central = BLE.central(); // Attente d'une connexion Bluetooth

  if (central) { // Si une connexion est établie
    Serial.print("Connecté à : ");
    Serial.println(central.address());

    while (central.connected()) { // Tant que la connexion est active
      dataChar.setValue("Hello, World!"); // Envoi d'une valeur sur la caractéristique GATT
      delay(1000);
    }

    Serial.print("Déconnecté de : ");
    Serial.println(central.address());
  }
}
