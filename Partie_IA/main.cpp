
/*
-recuperer trame => TTL serie						En cours
-decodage trame => pourvoir le change facilement	OK
-creation trame et envoie au robot					Nop

*/

#include <iostream>
#include <fstream>
#include <string>
#include <thread>
#include <unistd.h>

#include "Frame_manager.hpp"


using namespace std;

#define SEPARATOR		";"
#define NB_CHAMP		5

/**
 * Structure des données des robots
*/
typedef struct data_s {
	int QRdata;
	int x;
	int y;
	int rot;
	long timestamp;
} data_t;
	
class Frame_manager;

string recuperationTrame(string cheat = "");
data_t decodetrame(string trame);

//bool thread_running;
//void thread_func();
//fstream serialFile;


int main(int argc, char **argv)
{
	//  print("Code du QR : " + code.data.decode("utf-8") + " Pos : " + str(int(top_right[0])) + " " + str(int(top_right[1])) + " Orient : " + str(int(angle_degrees)))

	Frame_manager frame;
	frame.add("1;23;45;67;89");

	string last;
	while (1)
	{
		last = frame.getLast();
		if (last != "")
		{
			printf("\r\nNew frame: %s !!!\r\n", last.c_str());
			data_t data = decodetrame(last);

			printf("%d %d %d %d %ld\r\n", data.QRdata, data.x, data.y, data.rot, data.timestamp);
			
		}

		usleep(500000);
	}
	

	return (0);
}




string recuperationTrame(string cheat)
{
	string retstr;

	if (cheat == "")
	{
		
	}
	else
	{
	//simulation
		retstr = cheat;
	}

	return (retstr);
}

data_t decodetrame(string trame)
{
	data_t data;
	size_t pos		= 0;
	size_t lastPos	= 0;

	for(int cpt = 0;cpt < NB_CHAMP;cpt++)
	{
		pos = trame.find(SEPARATOR, pos+1);
		if (pos != string::npos)
		{
			/* SEPARATOR trouvé => decode */
			switch (cpt)
			{
				case 0: data.QRdata	= atoi(trame.substr(lastPos+1, pos-1).c_str() ); break;
				case 1: data.x		= atoi(trame.substr(lastPos+1, pos-1).c_str() ); break;
				case 2: data.y		= atoi(trame.substr(lastPos+1, pos-1).c_str() ); break;
				case 3: data.rot	= atoi(trame.substr(lastPos+1, pos-1).c_str() ); break;
				
				default: break;
			}
			lastPos = pos;
		}
		else
		{
			/* aucun SEPARATOR trouvé => remplie la derniere data puis exit */
			if (cpt == (NB_CHAMP-1))
			{
				data.timestamp = atoi(trame.substr(lastPos+1, trame.length() ).c_str() );
//				cout << "last cpt : " << cpt << ", " << trame.substr(lastPos+1, trame.length() ) << ", " << lastPos+1 << ", " << trame.length()  << endl;
			}
			break;
		}
//		cout << "cpt : " << cpt << ", " << trame.substr(lastPos+1, pos-1) << ", " << lastPos+1 << ", " << pos-1 << endl;
	}

	return (data);
}
