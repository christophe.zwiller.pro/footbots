from time import sleep
import time
import cv2
import math
import serial
from numpy import zeros,array
#from pyzbar.pyzbar import decode, ZBarSymbol
#from picamera import PiCamera
#from picamera.array import PiRGBArray

def read_qr_code(img):
    #try:
        #print("Ligne 12")
        detect = cv2.QRCodeDetector()

        retval, points = detect.detectMulti(img)

        if(retval):  #retvzl
            #print("Ligne 17")
            retval, decoded_info, straight_qrcode = detect.decodeMulti(img, points)
            #codes = decode(img, symbols=[ZBarSymbol.QRCODE])
            
           # tabOrient = zeros(len(points), int)
            angle_degrees = -1
            
            for i in range(len(points)) :
                center_x = (points[i][0][0] + points[i][2][0]) / 2
                center_y = (points[i][0][1] + points[i][2][1]) / 2

                diff_x = points[i][0][0] - center_x
                diff_y = points[i][0][1] - center_y
                angle_radians = math.atan2(diff_y, diff_x)
                angle_degrees = angle_radians * 180 / math.pi
                if angle_degrees < 0:
                    angle_degrees += 360
                
                try :
                    #cv2.putText(img, str(int(angle_degrees)) + "   " + str(int(decoded_info[i])), (int(points[i][0][0]), int(points[i][0][1])), cv2.FONT_HERSHEY_SIMPLEX,
                        #1, (0, 0, 255), 2, cv2.LINE_4)
                    #trame = b""+str(int(angle_degrees))+";"+str(int(decoded_info[i]))+";0;0;0\r"
                    
                    
                    print("angle : " + str(int(angle_degrees))+ "    info : " +(str(int(decoded_info[i]))))
                except :
                    #cv2.putText(img, str(int(angle_degrees)), (int(points[i][0][0]), int(points[i][0][1])), cv2.FONT_HERSHEY_SIMPLEX,
                        #1, (0, 0, 255), 2, cv2.LINE_4)
                    print("angle :" + str(int(angle_degrees)))



            #    print(int(decoded_info[i]) + type(int(decoded_info[i])) + "WEEEEEEEEEEEEE")
                    
            #    try :    
            #        tabOrient[int(decoded_info[i])] = int(angle_degrees)
            #    except :    
            #        tabOrient[int(decoded_info[i])] = -1
                   
            #for code in codes : 
            	#angle_degrees = -1
            	
            	#angle = tabOrient[code.data.decode("utf-8")]
            	#angle = 0
            	
            #	try :
            #		angle = tabOrient[code.data.decode("utf-8")]
            #		tabOrient[code.data.decode("utf-8")] = -1
            #	except :
            #		angle = -1
           
            	#print("Code du QR : " + code.data.decode("utf-8") + " Pos : " + str(int(100)) + " " + str(int(200)) + " Orient : " + str(int(angle)))
        else :
            print("ligne 64")
        return 0
    #except:
     #   print("error print")
      #  return

# Version PC
video_capture = cv2.VideoCapture(4)

# Version rasp
#camera = PiCamera()
#camera.resolution = (640*2, 480*2)
#camera.framerate = 10
#rawCapture = PiRGBArray(camera, size=(640*2, 480*2))

time.sleep(1)

#for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
#    read_qr_code(frame.array)
#    rawCapture.truncate(0)

ser = serial.Serial('/dev/ttyUSB0',115200)

while True:
    ret, frame = video_capture.read()
    frame = cv2.resize(frame,(1000,1000))

    trame =b"0;0;0;0;0\r"
    ser.write(trame)
    
    if(ret):
    	read_qr_code(frame)


    



    #cv2.imshow("Webcam", frame)

    #if(cv2.waitKey(25) & 0xFF == ord('q')) :
        #cv2.waitKey(0)
    sleep(0.1)

camera.close()
video_capture.release()
#cv2.destroyAllWindows()

